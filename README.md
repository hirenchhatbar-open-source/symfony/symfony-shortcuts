# Symfony shortcuts

## Installation

```bash
# Go to installation directory
cd /usr/local/share/

# Clone repository
git clone https://gitlab.com/hirenchhatbar-open-source/symfony/symfony-shortcuts.git symfony_shortcuts

# Add below line in ~/.bashrc or ~/.zshrc as per terminal you are using 
source /usr/local/share/symfony_shortcuts/aliases

# Close terminal and start again
```